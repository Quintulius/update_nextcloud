#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import re
from shutil import rmtree
import subprocess
import urllib
import zipfile


def convert_maintenance_value(maintenance):
    """
    Convert the raw maintenance value to boolean as accepted by config.php.

    :param maintenance: raw maintenance value.
    :type maintenance: bool or str
    :return: str
    """
    if isinstance(maintenance, bool):
        maintenance = str(maintenance).lower()
    elif not isinstance(maintenance, str):
        raise TypeError("'maintenance' must be of type 'bool' or 'str'.")
    if maintenance not in ['false', 'true']:
        raise ValueError("'maintenance' must be 'false' or 'true'.")
    return maintenance


def set_maintenance_local(config_path, maintenance):
    """
    Set the maintenance value of a local (as opposed to remote) config.php

    :param config_path: path of config.cfg.
    :param maintenance: raw maintenance value.
    :type config_path: str
    :type maintenance: bool or str
    """
    maintenance = convert_maintenance_value(maintenance)
    with open(config_path, "r") as f:
        config = f.read()
    config = re.sub(r"(?<='maintenance'\s=>\s)([a-z]*)(?=,)", maintenance, config)
    with open(config_path, "w") as f:
        f.write(config)


def set_maintenance_remote(server_credentials, config_path, maintenance, simulate=False):
    """
    Set the maintenance value of a remote (as opposed to local) config.php by modifying it locally.

    :param server_credentials: server credentials.
    :param config_path: path of config.cfg.
    :param maintenance: raw maintenance value.
    :param simulate: simulate or call lftp commands.
    :type server_credentials: str
    :type config_path: str
    :type maintenance: bool or str
    :type simulate: bool
    :return:
    """
    args = ['lftp', server_credentials, '-e', ' set xfer:clobber yes;get {} -o config_tmp.php;exit;'.
            format(config_path)]
    if not simulate:
        subprocess.call(args=args)
    set_maintenance_local("config_tmp.php", maintenance)
    args = ['lftp', server_credentials, '-e', 'put config_tmp.php -o {};exit;'.format(config_path)]
    if not simulate:
        subprocess.call(args=args)
    os.remove("config_tmp.php")


def extract_data_location(nextcloud_path):
    """
    Extract le location of the 'data' folder.

    :param nextcloud_path: path of the nextcloud directory.
    :type nextcloud_path: str
    :return: str
    """
    with open(os.path.join(nextcloud_path, "config", "config.php"), "r") as f:
        config = f.read()

    data_dir_regex = re.compile(r"'datadirectory'\s*=>\s*'(.*)',")
    match = data_dir_regex.search(config)
    if not match:
        return os.path.join(nextcloud_path, "data")
    else:
        return match.group(1)


def patch_htaccess(nextcloud_path):
    """
    Patch nextcloud/.htaccess

    :param nextcloud_path: path of the nextcloud directory.
    :type nextcloud_path: str
    """
    htaccess_file = os.path.join(nextcloud_path, ".htaccess")

    with open(htaccess_file, 'r') as f:
        htaccess = f.read()

    htaccess = htaccess.replace("Header set", "Header always set")
    end_header_part = "SetEnv modHeadersAvailable true"
    htaccess = htaccess.replace(end_header_part, "{}\n\t{}".format('Header always set Strict-Transport-Security '
                                                                   '"max-age=15768000; includeSubDomains; preload"',
                                                                   end_header_part))
    with open(htaccess_file, "w") as f:
        f.write(htaccess)


def send_command_to_server(cmd_args, simulate):
    if simulate:
        print("Calling: ".format(" ".join(cmd_args)))
    else:
        try:
            subprocess.call(args=cmd_args)
        except subprocess.CalledProcessError as e:
            print("Calling {} led to the following error:\n{}".format(" ".join(cmd_args), str(e)))


def main():
    parser = argparse.ArgumentParser(description="Nextcloud update on an shared OVH server")
    parser.add_argument('--ftp-address', type=str, help='Address of the ftp server')
    parser.add_argument('--ftp-login', type=str, help='Login of the ftp server')
    parser.add_argument('--ftp-password', type=str, help='ftp password')
    parser.add_argument('--nextcloud-dir', type=str, help='Path of the remote nextcloud directory')
    parser.add_argument('--nextcloud-version', type=str, default='latest', help='Nextcloud version to install')
    parser.add_argument('--new-nextcloud-name', type=str, default='new_nextcloud',
                        help='Temporary name of the new nextcloud direcory')
    parser.add_argument('--simulate', type=bool, default=False, help='Simulate the lftp commands')
    parser.add_argument('--pause-after-download', type=bool, default=False)
    args = parser.parse_args()
    ftp_address = args.ftp_address
    ftp_login = args.ftp_login
    ftp_password = args.ftp_password
    nextcloud_dir = args.nextcloud_dir
    nextcloud_version = args.nextcloud_version
    new_nextcloud_name = args.new_nextcloud_name
    simulate = args.simulate

    # Download and extract asked nextcloud version
    urllib.urlretrieve("https://download.nextcloud.com/server/releases/{}.zip".format(nextcloud_version),
                       "nextcloud.zip")
    with zipfile.ZipFile("nextcloud.zip", "r") as zip_ref:
        zip_ref.extractall()

    # Rename it
    if os.path.exists(new_nextcloud_name):
        rmtree(new_nextcloud_name)
    os.rename("nextcloud", new_nextcloud_name)

    # Patch it
    patch_htaccess(new_nextcloud_name)

    server_credentials = 'sftp://{}:{}@{}/home/{}'.format(ftp_login, ftp_password, ftp_address, ftp_login)

    # Add config.php and sent the new nextcloud version to the server
    cmd_args = ['lftp', server_credentials, '-e', 'get {}/config/config.php -o {}/config;mirror -R {};exit;'.format(
            nextcloud_dir, new_nextcloud_name, new_nextcloud_name)]
    send_command_to_server(cmd_args, simulate=simulate)

    if args.pause_after_download:
        raw_input("New nextcloud downloaded and patched. Press Enter to update...")

    # Move the data folder if inside nextcloud repository
    data_dir = extract_data_location(new_nextcloud_name)
    if nextcloud_dir in data_dir:
        new_data_dir = os.path.split(os.path.join(new_nextcloud_name, data_dir[len(nextcloud_dir):]))[0]
        cmd_args = ['lftp', server_credentials, '-e', 'mkdir -p {}; mv {} {}; exit;'.format(
                new_data_dir, data_dir, new_data_dir)]
        send_command_to_server(cmd_args, simulate=simulate)

    # Remove already existing nextcloud backup
    cmd_args = ['  s', server_credentials, '-e', 'rm -r -f {}_back; exit;'.format(nextcloud_dir)]
    send_command_to_server(cmd_args, simulate=simulate)

    # Put the old nextcloud into maintenance mode
    set_maintenance_remote(server_credentials, os.path.join(nextcloud_dir, "config", "config.php"), maintenance=True,
                           simulate=simulate)

    # Backup current nextcloud
    cmd_args = ['lftp', server_credentials, '-e', 'mv {} {}_back; exit;'.format(nextcloud_dir, nextcloud_dir)]
    send_command_to_server(cmd_args, simulate=simulate)

    # Setup new nextcloud
    cmd_args = ['lftp', server_credentials, '-e', 'mv {} {}; exit;'.format(new_nextcloud_name, nextcloud_dir)]
    send_command_to_server(cmd_args, simulate=simulate)

    # End the new nextcloud maintenance
    set_maintenance_remote(server_credentials, os.path.join(nextcloud_dir, "config", "config.php"), maintenance=False,
                           simulate=simulate)

    # Remove local files
    rmtree(new_nextcloud_name)
    os.remove("nextcloud.zip")
    
if __name__ == '__main__':
    main()
